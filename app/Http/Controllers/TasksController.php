<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TasksController extends Controller
{
    public function index(Request $request)
    {
        $tasks = $request->user()->tasks;
        return view('tasks.index', compact('tasks'));
    }

    public function store(Request $request)
    {
        $this->validate($request, ['task' => 'required']);
        
        $request->user()->tasks()->save(
            $task = new Task([
                'task' => $request->task
            ])
        );

        return $task;
    }
}
