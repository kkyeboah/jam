<?php

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
    Route::get('tasks', 'TasksController@index')->name('tasks');
    Route::post('tasks', 'TasksController@store')->name('tasks');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
